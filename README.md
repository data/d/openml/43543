# OpenML dataset: Sample---Employees-Monthly-Salary

https://www.openml.org/d/43543

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
This is a sample dataset to explore key insights, EDA, and to perform statistical simulations.
Content
This Dataset contains Gross and Net salary of each Employee with Tax deduction
Inspiration

Do male employees earn significantly more than female employees?
Are there any departments paying significantly low salaries in the organization?
What is the relation between gender and leadership roles in the organization?
What is the relation between age and leadership roles in the organization?
Are Data Scientists getting paid significantly more than others in this company?
Does the salary depend on Age?

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43543) of an [OpenML dataset](https://www.openml.org/d/43543). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43543/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43543/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43543/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

